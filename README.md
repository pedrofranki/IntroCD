# Projeto: Vieses na Indústria Cinematográfica

## Equipe: Data Awards

## Descrição: 

  O projeto tem como objetivo observar se os Oscar têm uma tendência em priorizar determinados grupos sociais em suas indicações e premiações.

## Membros: 

Isabela Vieira Santos, 2090317, @isavs, BSI, UTFPR

Pedro Henrique Belotto Frankiewicz, 1189212, @pedrofranki, BSI, UTFPR

Yasmin Moro, 2462370, @yasminmrf, BSI, UTFPR

